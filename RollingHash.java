
public class RollingHash {
	
	private int[] hashPrefix;
	private int[] hashPowersModInv;
	private int base;
	private int mod;
	
	/**
	 * Mod should be always prime and base smaller than mod.
	 */
	public RollingHash(String str, int base, int mod) throws IllegalArgumentException {
		this(str.toCharArray(), base, mod);
	}
	
	/**
	 * Mod should be always prime and base smaller than mod.
	 */
	public RollingHash(char[] text, int base, int mod) throws IllegalArgumentException {
		if (base<=0 || mod<=0) {
			throw new IllegalArgumentException("Base and mod must be positive.");
		}
		if (base>=mod) {
			throw new IllegalArgumentException("Base must be smaller than mod.");
		}
		this.base = base;
		this.mod = mod;
		precalcHashes(text);
	}
	
	private void precalcHashes(char[] text) {
		hashPrefix = new int[text.length];
		hashPowersModInv = new int[text.length];
		long pow = 1;
		for (int i=0; i<text.length; ++i) {
			hashPowersModInv[i] = modInverse((int)pow, mod);
			long h = (i==0 ? 0 : hashPrefix[i-1]) + pow * text[i];
			if (h>=mod) h %= mod;
			hashPrefix[i] = (int)h;
			pow *= base;
			if (pow>=mod) pow %= mod;
		}
	}
	
	/**
	 * Calculates hash for original_string.substring(start, end).
	 */
	public int getHash(int start, int end) {
		if (start<0 || end>hashPrefix.length) {
			throw new IllegalArgumentException("Arguments out of range.");
		}
		if (start>=end) {
			throw new IllegalArgumentException("Start cannot be larger than end.");
		}
		long diff = hashPrefix[end-1] - (start==0 ? 0 : hashPrefix[start-1]);
		if (diff<0) diff += mod;
		// (diff / p^start) in residue class for mod is diff*modInv(p^start,mod) % mod
		return (int)((diff * hashPowersModInv[start]) % mod);
	}
	
	/**
	 * Modular multiplicative inverse for prime modulo only.
	 * Based on little Fermat's theorem:
	 * a^(m-1) = 1 (mod m)
	 * Thus we can write:
	 * a * a^(m-2) = 1 (mod m)
	 * a^(m-2) = a^(-1) (mod m)
	 * So we just have to calculate a^(m-2) % m.
	 */
	private int modInverse(int a, int m) {
	    return powMod(a, m-2, m);
	}
	
	/**
	 * Calculates (a^b) % mod in O(log(b)).
	 */
	private int powMod(int a, int b, int mod) {
		long x = 1, y = a;
		while (b > 0) {
			if ((b&1) == 1) {
				x *= y;
				if (x>mod) x%=mod;
			}
			y *= y;
			if (y>mod) y%=mod;
			b >>= 1;
		}
		return (int)x;
	}
	
}
