Efficient implementation of the rolling hash in Java.
After initialization, hash of any substring of the original string can be computed in *O(1)* time.
Initialization takes *O(n\*log(n))* where *n* is the length of the original string.
