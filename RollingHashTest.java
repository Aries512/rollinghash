import static org.junit.Assert.*;
import java.util.Random;
import org.junit.Ignore;
import org.junit.Test;

public class RollingHashTest {

	private int getHashBruteForce(char[] s, int from, int to, int p, int mod) {
		int hash = s[from];
		long pow = 1;
		for (int i=from+1; i<to; ++i) {
			pow *= p;
			if (pow > mod) pow %= mod;
			hash = (int)((hash + s[i] * pow) % mod);
		}
		return hash;
	}

	private boolean testCaseComplete(String str, int p, int mod) {

		char[] s = str.toCharArray();
		RollingHash h = new RollingHash(s, p, mod);

		for (int i=0; i<s.length; ++i) for (int j=i+1; j<=s.length; ++j) {
			int hash1 = h.getHash(i, j);
			int hash2 = getHashBruteForce(s, i, j, p, mod);
			if (hash1 != hash2) return false;
		}

		return true;
	}

	private boolean testCaseRandom(String str, int p, int mod, int tries) {

		char[] s = str.toCharArray();
		RollingHash h = new RollingHash(s, p, mod);
		Random rnd = new Random(System.currentTimeMillis());
		
		for (int i=0; i<tries; ++i) {
			int from = rnd.nextInt(s.length);
			int to = from + 1 + rnd.nextInt(s.length-from);
			int hash1 = h.getHash(from, to);
			int hash2 = getHashBruteForce(s, from, to, p, mod);
			if (hash1 != hash2) return false;
		}

		return true;
	}
	
	private char[] makeRandomText(int length) {
		char[] s = new char[length];
		Random rnd = new Random(System.currentTimeMillis());
		for (int i=0; i<length; ++i) {
			s[i] = (char)rnd.nextInt(256);
		}
		return s;
	}

	private int[] bases = {31,11,17,10,5,1007};
	private int[] mods = {1000000007,1000000009,2000003,307};
	
	@Test
	public void testSmall() {
		for (int b: bases) for (int m: mods) if (m>b) {
			assertTrue(testCaseComplete("", b, m));
			assertTrue(testCaseComplete("a", b, m));
			assertTrue(testCaseComplete("aaaaaaaaaa", b, m));
			assertTrue(testCaseComplete("abcdefghijklmnopqrstuvwxyz", b, m));
			assertTrue(testCaseComplete("abbababbbababababababababbba", b, m));
			assertTrue(testCaseComplete("adsjlkhlasfkjhfsdlkjblfsdj", b, m));
		}
	}
	
	@Test
	@Ignore
	public void testBig() {
		for (int b: bases) for (int m: mods) if (m>b) {
			System.out.println("Testing base " + b + " with mod " + m + "...");
			assertTrue(testCaseRandom(new String(makeRandomText(10)), b, m, 1000));
			assertTrue(testCaseRandom(new String(makeRandomText(1000)), b, m, 1000));
			assertTrue(testCaseRandom(new String(makeRandomText(1000000)), b, m, 1000));
		}
	}

}
